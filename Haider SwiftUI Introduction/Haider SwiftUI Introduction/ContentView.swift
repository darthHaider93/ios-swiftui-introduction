//
//  ContentView.swift
//  Haider SwiftUI Introduction
//
//  Created by Haider Ashfaq on 20/06/2019.
//  Copyright © 2019 Haider Ashfaq. All rights reserved.
//

import SwiftUI

struct ContentView : View {
    var body: some View {
        Text("Hello World")
    }
}

#if DEBUG
struct ContentView_Previews : PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
#endif
